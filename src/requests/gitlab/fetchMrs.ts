import { Mr } from "../../models/Mr";

import { executeGitlabReq } from "./helpers";

const fetchAllOpenMrIds = async ({
  projectId,
  labels,
}: {
  projectId: string;
  labels: string[];
}): Promise<string[]> => {
  const response = await executeGitlabReq({
    projectId,
    params: {
      state: "opened",
      labels,
    },
  });
  const { data } = response;
  if (!data) {
    throw new Error("No data found for fetchAllOpenMrIds");
  }
  return data.map((x: { iid: string }) => x.iid);
};

const fetchMr = async ({
  projectId,
  id,
}: {
  projectId: string;
  id: string;
}): Promise<Mr> => {
  const response = await executeGitlabReq({
    projectId,
    path: `/${id}`,
  });
  const { data } = response;
  if (!data) {
    throw new Error(`No data found for fetchMr of id "${id}"`);
  }
  return data;
};

export const fetchAllOpenMrs = async ({
  projectId,
  labels,
}: {
  projectId: string;
  labels: string[];
}): Promise<Mr[]> => {
  const allOpenMrIds = await fetchAllOpenMrIds({ projectId, labels });
  const allOpenMrPromises = allOpenMrIds.map((id) =>
    fetchMr({ projectId, id })
  );
  const allOpenMrs = await Promise.all(allOpenMrPromises);
  return allOpenMrs;
};
