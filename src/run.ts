import fs from "fs-extra";

import { env } from "./env";
import { outDir, outFile } from "./constants";
import { fetchAllOpenMrs } from "./requests/gitlab/fetchMrs";
import { sendSlackMessage } from "./helpers/sendSlackMessage";

export const run = async (): Promise<void> => {
  const reposToBeSkipped = [];
  const MR_LIMIT = env.RENOVATE_INSPECTOR_MR_LIMIT;
  const MR_WARNING_LIMIT = Math.floor(MR_LIMIT * 0.75);

  console.log(
    `The limit for open RenovateBot MRs is ${MR_LIMIT}. Warnings issued at ${MR_WARNING_LIMIT}.`
  );

  for (const REPO of env.REPOS) {
    const { repo, channel } = REPO;
    const allOpenRenovateBotMrs = await fetchAllOpenMrs({
      projectId: encodeURIComponent(repo),
      labels: [env.RENOVATE_INSPECTOR_LABEL],
    });
    const amount = allOpenRenovateBotMrs.length;
    console.log(`Repo "${repo}" has ${amount} open RenovateBot MRs`);
    if (allOpenRenovateBotMrs.length > MR_LIMIT) {
      // Skip this repo when running RenovateBot.
      // Also, issue a warning that the repo has reached the limit of open MRs and RenovateBot will skip the repo until it is no longer above the limit.
      reposToBeSkipped.push(repo);
      sendSlackMessage({ type: "exceeded", repo, channel, amount });
    } else if (allOpenRenovateBotMrs.length >= MR_WARNING_LIMIT) {
      // Issue a warning that the repo is close to reaching the limit of open MRs and (if reached) RenovateBot will skip the repo until it is no longer above the limit.
      sendSlackMessage({ type: "approaching", repo, channel, amount });
    }
  }

  fs.ensureDirSync(outDir);
  fs.writeJSONSync(outFile, { reposToBeSkipped });
};
