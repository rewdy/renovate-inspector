import { InjectedEnv, Env } from "./types";
import { resolveApp } from "../utils";

const injectedEnv: InjectedEnv = process.env;

if (!injectedEnv.RENOVATE_CONFIG_FILE) {
  throw new Error("Could not resolve RENOVATE_CONFIG_FILE");
}
if (!injectedEnv.RENOVATE_TOKEN) {
  throw new Error("Could not resolve RENOVATE_TOKEN");
}

let renovateConfig: any;
try {
  renovateConfig = require(resolveApp(injectedEnv.RENOVATE_CONFIG_FILE));
} catch (error) {
  // do nothing. handle in next check.
}

if (!renovateConfig) {
  throw new Error(
    `Could not resolve renovate config at "${injectedEnv.RENOVATE_CONFIG_FILE}"`
  );
}

/* Ensure RENOVATE_TOKEN */

const RENOVATE_TOKEN = injectedEnv.RENOVATE_TOKEN;

if (typeof RENOVATE_TOKEN !== "string" || !RENOVATE_TOKEN) {
  throw new Error(`RENOVATE_TOKEN must be set`);
}

/* Ensure ENDPOINT */

const _ENDPOINT = renovateConfig.endpoint;

if (typeof _ENDPOINT !== "string" || !_ENDPOINT) {
  throw new Error(`"endpoint" must be set in your renovate config`);
}

// Remove the trailing slash if it exists
const ENDPOINT = _ENDPOINT.replace(/\/$/, "");

/* Ensure DRY_RUN */

const DRY_RUN = renovateConfig.dryRun;

if (typeof DRY_RUN !== "boolean") {
  throw new Error(`"dryRun" must be set in your renovate config`);
}

/* Ensure PLATFORM */

const PLATFORM = renovateConfig.platform;

if (typeof PLATFORM !== "string" || !PLATFORM) {
  throw new Error(`"platform" must be set in your renovate config`);
}

if (PLATFORM !== "gitlab") {
  throw new Error(`Only the gitlab platform is currently supported`);
}

/* Ensure REPOS */

const REPOS = renovateConfig.repos;

if (!Array.isArray(REPOS)) {
  throw new Error(`"repos" must be set in your renovate config`);
}

if (!REPOS.every((x) => typeof x === "object")) {
  throw new Error(
    `"repos" must be set in your renovate config to an array of objects`
  );
}
if (!REPOS.every((x) => x.repo && typeof x.repo === "string")) {
  throw new Error(
    `"repos" must be set in your renovate config to an array of objects with a "repo" property that must be set to a non-empty string`
  );
}
if (!REPOS.every((x) => x.channel && typeof x.channel === "string")) {
  throw new Error(
    `"repos" must be set in your renovate config to an array of objects with a "channel" property that must be set to a non-empty string`
  );
}

/* Ensure RENOVATE_INSPECTOR_LABEL */

const { RENOVATE_INSPECTOR_LABEL = "RenovateBot" } = injectedEnv;

if (typeof RENOVATE_INSPECTOR_LABEL !== "string" || !RENOVATE_INSPECTOR_LABEL) {
  throw new Error(`RENOVATE_INSPECTOR_LABEL must be set to a non-empty string`);
}

/* Ensure RENOVATE_INSPECTOR_ADDITIONAL_MESSAGE */

const { RENOVATE_INSPECTOR_ADDITIONAL_MESSAGE } = injectedEnv;

/* Ensure RENOVATE_INSPECTOR_MR_LIMIT */

const { RENOVATE_INSPECTOR_MR_LIMIT: _RENOVATE_INSPECTOR_MR_LIMIT = "10" } =
  injectedEnv;

const RENOVATE_INSPECTOR_MR_LIMIT = Number(_RENOVATE_INSPECTOR_MR_LIMIT);

if (
  Number.isNaN(RENOVATE_INSPECTOR_MR_LIMIT) ||
  !Number.isInteger(RENOVATE_INSPECTOR_MR_LIMIT) ||
  RENOVATE_INSPECTOR_MR_LIMIT <= 0
) {
  throw new Error(`RENOVATE_INSPECTOR_MR_LIMIT must be set to a valid integer`);
}

/* Ensure SLACK_WEBHOOK_URL */

const { SLACK_WEBHOOK_URL } = injectedEnv;

export const env: Env = {
  RENOVATE_TOKEN,
  ENDPOINT,
  DRY_RUN,
  PLATFORM,
  REPOS,
  RENOVATE_INSPECTOR_LABEL,
  RENOVATE_INSPECTOR_ADDITIONAL_MESSAGE,
  RENOVATE_INSPECTOR_MR_LIMIT,
  SLACK_WEBHOOK_URL,
};
