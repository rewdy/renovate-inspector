#!/usr/bin/env node

import "./prep";
import { env } from "./env";
import { run } from "./run";

if (env.DRY_RUN) {
  console.log("Skipping due to dry run.");
  process.exit(0);
}

if (!env.SLACK_WEBHOOK_URL) {
  console.warn(
    "SLACK_WEBHOOK_URL is not defined. No Slack messaging will occur."
  );
}

run();
